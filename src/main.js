import './assets/css/main.css'
function MainPage() {
    return (
      <div className='row text-left m-0 p-0 main pt-3' >
        <div className='col-3 row m-0 p-0'>
            <div className='col-12 main-b-color text-start p-0'>
                <p className='mt-1 pb-0 mb-2 mx-2 main-c-color'>profile</p>
                <hr className='white p-0 m-0' />
                <div className='m-3 panel-box1 main-b-color2'>

                </div>
                <div className='p-0 m-0 row ps-2'>
                    <div className='p-0 logo-box col-2 text-center ms-4'>
                        <img src='./images/profile.svg' height='80%' />
                    </div>
                    <div className='p-0 logo-box col-2 text-center'>
                        <img src='./images/profile.svg' height='80%' />
                    </div>
                    <div className='p-0 text-center logo-box col-2'>
                        <img src='./images/profile.svg' height='80%' />
                    </div>
                    <div className='p-0 text-center logo-box col-2'>
                        <img src='./images/profile.svg' height='80%' />
                    </div>
                    <div className='p-0 text-center col-2'>
                        <img src='./images/profile.svg' height='80%' />
                    </div>
                </div>
                <div className='end-box1 mx-3 mt-3 row'>
                    <hr className='white p-0 m-0' />
                    <span className='col-6 main-c-color2 text-center mt-2 mb-2 text-uppercase'>adas</span>
                    <span className='col-6 white text-center  mt-2 mb-2'>adas</span>
                    <hr className='white p-0 m-0' />
                    <p className='col-12 white text-size-s'>adas</p>
                    <hr className='white p-0 m-0' />
                </div>
            </div>
        </div>
        <div className='col-3 row m-0 p-0 ps-3 pe-2 panel-2'>
            <div className='col-12 main-b-color first-div'>

            </div>
            <div className='col-12 main-b-color second-div mt-3'>

            </div>
        </div>
        <div className='col-3 row m-0 p-0 pe-3 ps-2'>
            <div className='col-12 main-b-color first-div'>

            </div>
            <div className='col-12 main-b-color second-div mt-3'>

            </div>
        </div>
        <div className='col-3 row m-0 p-0'>
            <div className='col-12 main-b-color text-start p-0'>
                <p className='mt-1 pb-0 mb-2 mx-2 main-c-color'>result</p>
                <hr className='white p-0 m-0' />
                <div className='row p-0 m-0 px-2 '>
                    <div className='col-6 mt-2 mb-2 panel4-box'>
                        <img src='./images/swiss.png' height='30%' />
                        <span className='ms-2 main-c-color3'>name</span>
                        <p className = 'white p-0 m-0'>describ</p>
                    </div>
                    <div className='col-6 mt-2 mb-2'>
                        <p className = 'main-c-color3 p-0 m-0'>example</p>
                        <p className = 'white p-0 m-0'>describ</p>
                    </div>
                    <hr className='white p-0 m-0' />
                </div>
                <div className='row p-0 m-0 px-2 '>
                    <div className='col-6 mt-2 mb-2 panel4-box'>
                        <img src='./images/swiss.png' height='30%' />
                        <span className='ms-2 main-c-color3'>name</span>
                        <p className = 'white p-0 m-0'>describ</p>
                    </div>
                    <div className='col-6 mt-2 mb-2'>
                        <p className = 'main-c-color3 p-0 m-0'>example</p>
                        <p className = 'white p-0 m-0'>describ</p>

                    </div>
                    <div className='col-6'></div>
                    <div className='col-6 p-0 m-0' id="myProgress">
                        <div id="myBar"></div>
                    </div>
                    <hr className='white p-0 m-0' />
                </div>
            </div>
        </div>
      </div>
    );
  }
  
  export default MainPage;