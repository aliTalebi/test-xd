import '../assets/css/header.css'
function Header() {
    return (
      <div className='header row text-left m-0 p-0' >
        <div className='col-2 text-start mt-2'>
            <img src='./images/profile.svg' height='50%' />
        </div>
        <div className='col-8 mt-2'>
            <p className='text-center'>Site Header</p>
        </div>
        <div className='col-2 mt-2 text-end row'>
            <div className='col-6'></div>
            <div className='position-relative col-3 text-end'>
                <img className='position-absolute fit-edite-logo-pen' src='./images/edite.svg' height='48%' />
                <img className='position-absolute fit-edite-logo' src='./images/Path 3676.png' height='50%' />
            </div>
            <div className='position-relative col-3 text-start px-4'>
                <img className='position-absolute center-pic' src='./images/setting.svg' height='47%' />
                {/* <img className='position-absolute' src='./images/Path 3702.png' height='55%' /> */}
            </div>

        </div>
      </div>
    );
  }
  
  export default Header;