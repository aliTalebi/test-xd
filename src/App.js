
import './App.css';
import './assets/css/public.css';
import 'bootstrap/dist/css/bootstrap.min.css';
import Footer from './components/footer';
import Header from './components/Header';
import MainPage from './main';
function App() {
  return (
    <div className="App row p-0 m-0 justify-content-md-center">
      <Header />
      <MainPage />
      <Footer />
    </div>
  );
}

export default App;
